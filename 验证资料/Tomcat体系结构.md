# tomcat体系结构说明

tomcat体系图如下:



![image-20201229090431925](Tomcat%E4%BD%93%E7%B3%BB%E7%BB%93%E6%9E%84.assets/image-20201229090431925.png)



tomcat各个模块之间有嵌套的父子关系，如果使用配置文件来描述，可以大致简化为如下：

其中：

1. server组件是管理tomcat实例的组件，可以监听一个端口，从此端口上可以远程向该实例发送shutdown关闭命令。
2. service组件是一个逻辑组件，用于绑定connector和container，有了service表示可以向外提供服务，就像是一般的daemon类服务的service。可以认为一个service就启动一个JVM，更严格地说，一个engine组件才对应一个JVM(定义负载均衡时，jvmRoute就定义在Engine组件上用来标识这个JVM)，只不过connector也工作在JVM中。
3. connector组件是监听组件，它有四个作用：
   (1).开启监听套接字，监听外界请求，并和客户端建立TCP连接;
   (2).使用protocolHandler解析请求中的协议和端口等信息，如http协议、AJP协议;
   (3).根据解析到的信息，使用processer将分析后的请求转发给绑定的Engine;
   (4).接收响应数据并返回给客户端。
4. engine容器用于从connector组件处接收已建立的TCP连接，还用于接收客户端发送的http请求并分析请求，然后按照分析的结果将相关参数传递给匹配出的虚拟主机。engine还用于指定默认的虚拟主机。
5. host容器定义虚拟主机，由于tomcat主要是作为servlet容器的，所以为每个webapp指定了它们的根目录appBase。
6. context容器主要是根据path和docBase获取一些信息，将结果交给其内的wrapper组件进行处理(它提供wrapper运行的环境，所以它叫上下文context)。一般来说，都采用默认的标准wrapper类，因此在context容器中几乎不会出现wrapper组件。
7. wrapper容器对应servlet的处理过程。它开启servlet的生命周期，根据context给出的信息以及解析web.xml中的映射关系，负责装载相关的类，初始化servlet对象init()、执行servlet代码service()以及服务结束时servlet对象的销毁destory()。
8. executor组件为每个Service组件提供线程池，使得各个connector和Engine可以从线程池中获取线程处理请求，从而实现tomcat的并发处理能力。一定要注意，Executor的线程池大小是为Engine组件设置，而不是为Connector设置的，Connector的线程数量由Connector组件的acceptorThreadCount属性来设置。如果要在配置文件中设置该组件，则必须设置在Connector组件的前面，以便在Connector组件中使用`executor`属性来引用配置好的Executor组件。

