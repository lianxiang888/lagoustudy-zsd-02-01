package server.config;

/**
 * @program: webapps
 * @description: 配置信息
 * @author: zhoushiduo
 * @create: 2020-12-24 11:24
 */
public class WebConfig {

    //网站管理目录
    private static String rootWebApp = "root";

    public static String getRootWebApp() {
        return rootWebApp;
    }

    public static void setRootWebApp(String rootWebApp) {
        WebConfig.rootWebApp = rootWebApp;
    }
}
