package server.bean;

import server.HttpServlet;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 * @Author: zhoushiduo
 * @Date: 2020/12/26 0:10
 */

public class Context{
    private String name;
    private Map<String, Wrapper> wrapperMap = new HashMap<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Wrapper> getWrapperMap() {
        return wrapperMap;
    }

    public void setWrapperMap(Map<String, Wrapper> wrapperMap) {
        this.wrapperMap = wrapperMap;
    }
}
