package server.bean;

import server.HttpServlet;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 * @Author: zhoushiduo
 * @Date: 2020/12/26 0:11
 */

public class Host {
    private String name;
    private String appBase = "E:/3book/b9bigdata/lagoustudys/lagoustudy-zsd-02-01/code/webapps/Minicat/target/classes/webapps";
    private Map<String, Context> contextMap = new HashMap<>();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAppBase() {
        return appBase;
    }

    public void setAppBase(String appBase) {
        this.appBase = appBase;
    }

    public Map<String, Context> getContextMap() {
        return contextMap;
    }

    public void setContextMap(Map<String, Context> contextMap) {
        this.contextMap = contextMap;
    }
}
