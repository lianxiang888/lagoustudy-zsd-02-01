package server.bean;

import server.HttpServlet;

/**
 * @Description:
 * @Author: zhoushiduo
 * @Date: 2020/12/26 22:46
 */

public class Wrapper {
    private String name;
    private HttpServlet httpServlet;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HttpServlet getHttpServlet() {
        return httpServlet;
    }

    public void setHttpServlet(HttpServlet httpServlet) {
        this.httpServlet = httpServlet;
    }
}
