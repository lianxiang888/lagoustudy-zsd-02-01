package server.bean;

import server.HttpServlet;

import java.util.HashMap;
import java.util.Map;

/**
 * @Description:
 * @Author: zhoushiduo
 * @Date: 2020/12/24 22:39
 */

public class Mapper {

    private Host host;

    public Host getHost() {
        return host;
    }

    public void setHost(Host host) {
        this.host = host;
    }
}
