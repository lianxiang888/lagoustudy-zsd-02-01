package server;

import server.bean.Context;
import server.bean.Mapper;

import java.io.InputStream;
import java.net.Socket;
import java.util.Map;

public class RequestProcessor extends Thread {

    private Socket socket;
    private Mapper mapper;

    public RequestProcessor(Socket socket, Mapper mapper) {
        this.socket = socket;
        this.mapper = mapper;
    }

    @Override
    public void run() {
        try{
            InputStream inputStream = socket.getInputStream();

            // 封装Request对象和Response对象
            Request request = new Request(inputStream);
            Response response = new Response(socket.getOutputStream());

            // 静态资源处理

            Map<String, Context> contextMap = this.mapper.getHost().getContextMap();
            Context context = contextMap.get(request.getContext());
            if(context == null || context.getWrapperMap().get(request.getUrl()) == null) {
                String urlPath = this.mapper.getHost().getAppBase() + request.getUrl();
                response.outputHtml(urlPath);
            }else{
                // 动态资源servlet请求
                HttpServlet httpServlet = context.getWrapperMap().get(request.getUrl()).getHttpServlet();
                httpServlet.service(request,response);
            }
            socket.close();

        }catch (Exception e) {
            e.printStackTrace();
        }

    }
}
